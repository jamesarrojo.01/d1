Git Commands:

mkdir - make directory/folder
	ex. mkdir batch-165
cd - change directory
	ex. cd
pwd - print working directory
ls - list
touch - create files
	ex. touch discussion.txt



mkdir d1 && cd d1

=====
SSH Key
=====

1. Open a terminal
	Mac and Linux
		open the "Terminal" program
	Windows
		open the "Windows Terminal" or "Git Bash"

2. Create an SSH key
	ssh-keygen
3. Copy the SSH key
4. 